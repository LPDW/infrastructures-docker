<?php
$sapiType = php_sapi_name();
switch ($sapiType) {
    case 'apache2handler':
        include 'info_mod-php.html';
        break;
    case 'fpm-fcgi':
        include 'info_php-fpm.html';
        break;
}
